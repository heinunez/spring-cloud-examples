package com.example.demo.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.CurrencyConversionBean;

/*Con Ribbon ya no es necesario la url harcodeada
 * 
 * @FeignClient(name = "currency-exchange-service", url = "localhost:8000")
 */

/*Con Zuul apuntamos al gateway en vez del servicio
 * 
 * @FeignClient(name = "currency-exchange-service")
 */
@FeignClient("netflix-zuul-api-gateway-server")
@RibbonClient(name = "currency-exchange-service")
public interface CurrencyExchangeServiceProxy {

	/*
	 * Con Zuul es necesario agregar el nombre del servicio en el mapping
	 * 
	 * @GetMapping("/currency-exchange/from/{from}/to/{to}")
	 */
	@GetMapping("/currency-exchange-service/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversionBean retrieveExchangeValue(@PathVariable("from") String from,
			@PathVariable("to") String to);

}
