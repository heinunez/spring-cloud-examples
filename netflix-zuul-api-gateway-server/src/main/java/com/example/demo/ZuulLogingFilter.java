package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class ZuulLogingFilter extends ZuulFilter {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Object run() {
		HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
		logger.info("request uri -> {}",
				request.getRequestURI());
		return null;
	}

	// Logica para filtrar o no
	@Override
	public boolean shouldFilter() {
		return true;
	}

	// Prioridad de los filtros
	@Override
	public int filterOrder() {
		return 1;
	}

	// pre post error
	@Override
	public String filterType() {
		return "pre";
	}

}
